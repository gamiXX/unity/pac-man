using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public int score = 100;
    public int highScore;

    private bool isPaused = false;
    private bool isDead = false;

    [SerializeField]
    private TMP_Text scoreT;
    [SerializeField]
    private TMP_Text highscoreT;
    [SerializeField]
    private TMP_Text titleT;
    [SerializeField]
    private TMP_Text startT;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;

        UpdateScore();
    }

    public void quitClicked()
    {
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;
    }
    public void startClicked()
    {
        PlayerPrefs.SetInt("score", 0);
        Time.timeScale = 1;

        showUI(false);
    }

    void gameover()
    {
        titleT.text = "Game Over !";
        startT.text = "Restart";
        PlayerPrefs.SetInt("score", 0);
        Time.timeScale = 0;
        showUI(true);

    }

    void showUI(bool x)
    {
        Canvas canvas = GetComponent<Canvas>();
        canvas.enabled = x;

    }

    // Update is called once per frame
    void Update()
    {
        if (score > highScore)
        {
            PlayerPrefs.SetInt("highScore", score);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
            Time.timeScale = Convert.ToSingle(!isPaused);
            showUI(isPaused);
        }
        UpdateScore();
        if (isDead = PlayerPrefs.GetString("isDead").Equals("true"))
        {
            gameover();
            PlayerPrefs.SetString("isDead", "false");
        };

    }
    void UpdateScore()
    {
        score = PlayerPrefs.GetInt("score");
        highScore = PlayerPrefs.GetInt("highScore");

        scoreT.text = score.ToString();
        highscoreT.text = highScore.ToString();
    }
}
