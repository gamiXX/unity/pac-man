using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class pacman : MonoBehaviour
{
    private Rigidbody pacMan;
    public float speed;
    public float force;
    private bool isCollWall;

    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        pacMan = GetComponent<Rigidbody>();
        direction = Vector3.down;
    }
    // Start is called before the first frame update
    void Update()
    {
        direction = getPushedButton();
        pacMan.velocity = direction *speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == ("coin"))
        {
            Debug.Log("yes");
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag.Equals("wall"))
        {

            isCollWall = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag.Equals("wall"))
        {
            isCollWall = false;
        }
    }

    private Vector3 getPushedButton()
    {
        if (Input.GetAxis("Horizontal")>0)
        {
            return direction = Vector3.right;
        }
        else if((Input.GetAxis("Horizontal") < 0))
        {
            return direction = Vector3.left;
        }
        if(Input.GetAxis("Vertical")>0)
        {
           return direction = Vector3.forward;
        }
        else if(Input.GetAxis("Vertical")<0)
        {
            return direction = Vector3.back;
        }

        return direction;
    }

}
