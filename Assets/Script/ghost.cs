using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class ghost : MonoBehaviour
{
    private NavMeshAgent agent;
    public Transform player;
    public Transform initPos;

    private int isRunningAway = 1;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>(); 
        initPos = transform;
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(isRunningAway * player.position);
        //agent.SetDestination(-player.position);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.position == player.position)
        {
            //StartCoroutine(DisableEnemy(gameObject));

            Debug.Log("Game OVer");
            //gameObject.transform.position = initPos.position;
            PlayerPrefs.SetString("isDead","true");

            isRunningAway = -1;
            InvokeRepeating("Launch", 10.0f,0);
        }
    }
    void Launch()
    {
        Debug.Log("corr");
        isRunningAway = 1;


    }

    IEnumerator DisableEnemy(GameObject enemy)
    {
        enemy.SetActive(false);
        Debug.Log("Start counterr");

        yield return new WaitForSeconds(3f);

        Debug.Log("end counterr");
        enemy.SetActive(true);
    }

}
